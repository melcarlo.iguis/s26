/*
	1. What directive is used by Node.js in loading the modules it needs
			-require

	2. What Node.js module contains a method for server creation?
			
			-http module
			
	3. What is the method of the http object responsible for creating a server 

			-createServer

	4. What method is responsive object allows us to set status codes and content types?

			-writeHead

	5. Where will console.log() output its content when run in Node.js?

			-at the terminal (git bash)

	6. What property of the request object contains the address' endpoint?

			- request.url
*/


