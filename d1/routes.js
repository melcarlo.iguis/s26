const http = require('http');
const PORT = 4000;


http.createServer(function(request, response){

	if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type':'text/plain'})
		response.end('This is the homepage')
	}
	else if (request.url == '/register') {
		response.writeHead(200, {'Content-Type':'text/plain'})
		response.end('This is our Register Page')
	}
	else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Error : Page Not Found')
	}

}).listen(PORT);

console.log(`server is running at localhost: ${PORT}.`)